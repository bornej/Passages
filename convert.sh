#!/bin/sh
unprocessed=./Unprocessed
flacs=./flacs
tags=./tags
for source in $unprocessed/*
do
    # get the file "source" prefix
    source_basename=$(echo "$source" | sed -r "s/.+\/(.+)\..+/\1/")
    flac $source -o $flacs/$source_basename.flac
    #touch $tags/$source_basename.json
done
