---
title: "Uldor"
duration: "5:43"
description: "Uldor, a Melt01 variation"
artist: "Borne Jonathan"
album: "Passages"
year: "2018"
genre: "Electro-acoustic"
copyright: "Uldor Copyright (C) 2018  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Passages/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Uldor"
---
