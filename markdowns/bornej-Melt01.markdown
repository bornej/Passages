---
title: "Melt01"
duration: "3:35"
description: "Melt01 is Passages's project first song"
artist: "Borne Jonathan"
album: "Passages"
year: "2018"
genre: "Electro-acoustic"
copyright: "Melt01 Copyright (C) 2018  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Passages/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Melt01"
---
