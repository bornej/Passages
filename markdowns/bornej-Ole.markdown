---
title: "Ole"
duration: "3:23"
description: "Ole"
artist: "Borne Jonathan"
album: "Passages"
year: "2020"
genre: "Electro-acoustic"
copyright: "Ole Copyright (C) 2020  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Passages/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Ole"
---
